# include <iostream>
using namespace std;

# include <cmath>

      
# define TxtAttrib(x) SetConsoleTextAttribute(hConsole, x)
# define TagColor 11
# define loop_limit 5
# define dtab 7
# define fix 2
# define Tagging {					\
    *basic_var_tag=0;					\
    for(i=1; i<I; i++) *(basic_var_tag+i)=i+diff+48;	\
  }
        
# define TagPrint {					\
    printf("%*c", dtab,32);				\
    TxtAttrib(TagColor);				\
    for(j=1; j<J; j++) printf("x%*d", -dtab+1, j);	\
    printf("RHS\n");					\
  }

enum method {
  halt=0,
  maximize='1',
  minimize='2',
  dualmini='3',
  integer='4'
};

float* table;

class Simplex {
private:
  unsigned short I; //i_max
  unsigned short J; //j_max
  short diff;
  
  unsigned short pivot_i, pivot_j;
  unsigned short i, j;
  
  float pivot;
  float Temp, Tempreg;
  
  char* basic_var_tag;
  float* TTemp;
    
  char flag1strun;
  char loop_count;
  
public:	
  Simplex();	
  float* insert();
  void show(float*);
  
  void next_table(float*, float*);
  
  float* basic(float*, char);
  float* dual(float*);
  float* cuttingPlane(float *);
  
  void menu(char);
  char TestMode(char);
};

Simplex::Simplex(){}

float* Simplex::insert() {    
  cout<<"Size of the matrix\nI: "; cin>>I;
  cout<<"J: "; cin>>J;
  cls;
  
  //Mem Allocation
  TTemp=(float *)calloc(sizeof(float), I*J);
  
  free(basic_var_tag);
  basic_var_tag=(char *)calloc(sizeof(char), I);
  
  //69
  diff=J-I;    
  if(diff<0) diff=(-diff);
  
  Tagging;
  TagPrint;
  
  for(i=0; i<I; i++) {
    printf("x%c", *(basic_var_tag+i));
    TxtAttrib(7);     
    for(j=0; j<J; j++) {                     
      gotoxy((j*dtab)+5, 1+i);
      cin>>*(TTemp+i*J+j);
      gotoxy((j*dtab)+2, 1+i);
      if(j==J-1) printf(" %c", 221 );                 
      printf("%*.*f", dtab, fix, *(TTemp+i*J+j));
    }        
    cout<<endl;
    TxtAttrib(TagColor);
  }
  return TTemp;
}


int dtemp=1;
int ret;

int Dec2Fra(float dec_part) {
  int int_part=(int)dec_part;
  dec_part-=int_part;
  
  if(dec_part<0.005) return int_part;
  
  ret=Dec2Fra(1/dec_part);

  int_part*=ret;
  int_part+=dtemp;
  
  dtemp=ret;
  return int_part;    
}

char pfrac[6];
void pno(float no)
{    
  /*
    if(no==0 || fabs(no) <.0005)
    {
    printf("%*s", dtab, "0");
    return;
    }//*/
  //*
  float diff=no-(int)no;
  if(diff!=0 && fabs(diff)>.0005) {
    dtemp=1;
    if(no<0) sprintf(&pfrac[0], "-%d/%d", Dec2Fra(-no), dtemp);
    else sprintf(&pfrac[0], "%d/%d", Dec2Fra(no), dtemp);
    printf("%*s", dtab, pfrac);
  }     
  else //*/
    printf("%*.*f", dtab, fix, no);
}
  
void Simplex::show(float* T) {
  TagPrint;
  
  for(i=0; i<I; i++) {
    printf("x%c", *(basic_var_tag+i));
    TxtAttrib(7);
    for(j=0; j<J; j++) {
      if(j==J-1) printf(" %c", 221 );                 
      pno(*(T+i*J+j));
    }
    cout<<endl;
    TxtAttrib(TagColor);
  }
  cout<<endl;
}

float* Simplex::cuttingPlane(float *T) {}

float* Simplex::dual(float* T1) {    
  flag1strun=1;
  loop_count=0;
  
  float* T2= (float*)calloc(sizeof(float), I*J);
  
  Tagging;
  
re:
  Temp=*(T1+J+(J-1));
  pivot_i=1;
  
  for(i=2; i<I; i++) {
    if(Temp>* (T1+i*J+(J-1))) {
      Tempreg=Temp;
      pivot_i=i;
    }
  }
     
  if(Temp>0) return T1;
  
  Tempreg=*T1 / *(T1+pivot_i*J);  if(Tempreg<0) Tempreg*=(-1.0);
  pivot_j=0;
  for(j=1; j<diff; j++) {
    Temp=*(T1+j)/ *(T1+pivot_i*J+j); if(Temp<0) Temp*=(-1.0);
    if(Tempreg> Temp) {
      Temp=*(T1+j);
      pivot_j=j;
    }
  }
  
  Simplex::next_table(T1,T2);
  
  loop_count++;
  if(loop_count>loop_limit) {
    printf("Get OFF !! off limit");
    return 0;
  }
  
  if(flag1strun==1) { //1st run only
    T1=T2;
    T2=(float* )calloc(sizeof(float), I*J);
    flag1strun=0;
    goto re;
  }

  TTemp=T2; T2=T1; T1=TTemp;
  goto re;
}

float* Simplex::basic(float* T1, char ch) {    
  flag1strun=1;
  loop_count=0;
  
  float* T2= (float*)calloc(sizeof(float), I*J);
  
  Tagging;
 re:
  Temp=*T1;
  pivot_j=0;
  
  //finding pivot     
  if(ch==maximize) {//Maximize
    for(j=1; j<diff; j++) {
      if(Temp> *(T1+j)) {
	Temp=*(T1+j);
	pivot_j=j;
      }
    }
    
    //cheaking if its the -ve or not to continue opitmization
    if(Temp>=0) return T1;
  }
  else {
    if(*(T1+J-1)==0) return T1;         
    for(j=1; j<J-1; j++)
      //for(j=1; j<diff; j++) //change for 2 phase
      {
	if(Temp< *(T1+j)) {
	  Temp=*(T1+j);
	  pivot_j=j;
	}
      }
    //cheaking if its the +ve or not to continue opitmization
    if(Temp<=0) return T1;
  }     
     
  i=0;     
 reload:
  i++; 
  if(i<I) {
    if(*(T1+i*J+(J-1))==0) goto reload;
    Tempreg=*(T1+i*J+(J-1)) / *(T1+i*J+pivot_j);
    if(Tempreg<0) goto reload;     
  }
  else return T1;
  
  pivot_i=i;
  for(i++; i<I; i++) {
    if(*(T1+i*J+(J-1))<=0 || *(T1+i*J+pivot_j)<0) continue;
    
    Temp=*(T1+i*J+(J-1)) / *(T1+i*J+pivot_j);
    if(Tempreg > Temp) {
      Tempreg=Temp;
      pivot_i=i;                        
    }
  }
     
  Simplex::next_table(T1,T2);
  
  loop_count++;
  if(loop_count>loop_limit) {
    printf("Get OFF !! off limit");
    return 0;
  }     
  
  if(flag1strun==1) { //1st run only
    T1=T2;
    T2=(float* )calloc(sizeof(float), I*J);
    flag1strun=0;
    goto re;
  }
  
  TTemp=T2; T2=T1; T1=TTemp;
  goto re;
}

void Simplex::next_table(float *T1, float *T2) {     
  *(basic_var_tag+pivot_i)=pivot_j+47+diff;
  pivot=*(T1+pivot_i*J+pivot_j);
  
  findcursor(&x,&y);
  gotoxy((pivot_j*dtab)+2, y-I+pivot_i-1);
  TxtAttrib(10);     
  
  pno(pivot);
  //printf("i: %d j: %d %9.4f", pivot_i, pivot_j, pivot);
  TxtAttrib(7);
  gotoxy(x,y);
  
  for(i=0; i<I; i++) {
    for(j=0; j<J; j++) {			      
      if(pivot_i==i) *(T2+i*J+j)=(*(T1+i*J+j))/pivot;
      else
	*(T2+i*J+j)= *(T1+i*J+j) - 1.0*( *(T1+pivot_i*J+j)/pivot*(*(T1+i*J+pivot_j)));
    }
  }
         
  Simplex::show(T2);
}

int main() {
  Simplex s;
  s.menu(s.TestMode('3'));
  return 0;
}

void Simplex::menu(char ch) {
  float* Solution;
  for(;;ch=0) {
    cls;	
    cout<<"Simplex Method\n"
	<<"\n0. Fill the table\n"
	<<"\n1. Max Optimizition"
	<<"\n2. Min Optimizition"
	<<"\n3. Min Dual Method\n"
	<<"\n*. Move solution to reprocess"
	<<"\n\nEnter your choice: ";
		
    if(ch==0)
    def: ch=getch();        
        	
  testM: switch(ch) {			
    case '0':
      cls;
      free(table);
      table=Simplex::insert();
      break;
      
    case '1':
    case '2':
      cls;
      Simplex::show(table);
      free(Solution);
      Solution=Simplex::basic(table,ch);
      getch();
      break;
      
    case '3':
      cls;
      Simplex::show(table);
      free(Solution);
      Solution=Simplex::dual(table);
      getch();
      break;
      
    case '.':
      cls;
      Simplex::show(Solution);
      getch();
      break;
      
    case '`':
      ch=TestMode(0);               
      goto testM;
      break;
      
    case '*':
      free(table);
      table=Solution;
      break;
      
    case 13:
      cls;
      goto extended_view;
      
    case 27: return;
      
    default: //for blinking
      goto def;
    };
  back:	TxtAttrib(7);
  }
  
 extended_view:
  Simplex::show(table);
  switch(getch()) {
  case '-':
    if(J==2) break;
    J--;
    j=I*J;
    for(i=J-1; i<j; i++) *(table+i) = *(table+i + (i+1)/J);                    
    TxtAttrib(5);
    cout<<"Col Kill\n";
    goto extended_view;
    
  case 'i':
    printf("<i.j=f>");
    scanf("%d.%d=%f", &i, &j, &Temp);
    *(table+i*J+j)=Temp;
    goto extended_view;
    
  case 'I':
    printf("<i.j=x/y>");
    //scanf("%d.%d=%d %d", &x, &y, &i, &j);
    cin>>i>>j>>x>>y;
    *(table+i*J+j)= 1.0*x/y;
    goto extended_view;
    
  case 'r':
    printf("r:");
    cin>>x;
    for(i=0; i<J; i++) cin>>*(table+x*J+i);
    goto extended_view;
    
  case 's':
  case 'S':
    printf("swap 'c' or 'r'");
    if(getch()=='c') {
      for(x=0; x<I; x++) //using x as varible for loop
	{
	  Temp=*(table+x*J+i);
	  *(table+x*J+i) = *(table+x*J+j);
	  *(table+x*J+j) = Temp;
	}
    }
    else {
      for(x=0; x<J; x++) { //using x as varible for loop
	Temp=*(table+i*J+x);
	*(table+i*J+x) = *(table+j*J+x);
	*(table+j*J+x) = Temp;
      }
    }  
    goto extended_view;
  };
  goto back;
}

float sim_max[5][7] = {
  -5, -4,  0,  0,  0,  0,  0,
   6,  4,  1,  0,  0,  0, 24,
   1,  2,  0,  1,  0,  0,  6,
  -1,  1,  0,  0,  1,  0,  1,
   0,  1,  0,  0,  0,  1,  2
};

float sim_min1[4][7] = {
   7,  4,  0,  1,  0,  0,  9,
   3,  1,  0,  0,  1,  0,  3,
   1,  2,  1,  0,  0,  0,  4,
   4,  3,  0, -1,  0,  1,  6
};

float sim_min2[4][7] = { //2 Phase
  -2,  0,  1,  0,  0,  0,  1,
   1, -2,  1,  1,  0,  0, 11,
  -4,  1,  2,  0,  1,  0,  3,
  -2,  0,  1,  0,  0,  1,  1
};
        
float sim_min3[4][6] = {
   4, -2,  0,  1,  1, 12,
   3, -2,  0,  1,  0, 10,
   0,  1,  0,  0,  1,  1,
  -2,  0,  1,  0,  0,  1
};

float dual_min[4][6] = {
  -3, -2,  0,  0,  0,  0,
  -3, -1,  1,  0,  0, -3,
  -4, -3,  0,  1,  0, -6,
   1,  1,  0,  0,  1,  3
};

float cut_plane1[4][9] = {
   7,  7, -1, -1, -1,  0,  0,  0, 14,
   3,  1, -1,  0,  0,  1,  0,  0,  2,
   1,  4,  0, -1,  0,  0,  1,  0,  5,
   3,  2,  0,  0, -1,  0,  0,  1,  7
};

float cut_plane2[3][5] = {
  -7, -9,  0,  0,  0,
  -1,  3,  1,  0,  6,
   7,  1,  0,  1, 35,
};

char Simplex::TestMode(char ch) {
  printf("%c>Test Mode\n", 13);
  if(ch==0) ch=getch();
  free(table);    
  switch(ch) {
  case '0':
    I=4; J=6;
    table=&sim_min3[0][0];                        
    ch=minimize;
    break;
            
  case '1':
    I=5; J=7;            
    table=&sim_max[0][0];
    ch=maximize;
    break;            

  case '2':
    I=4; J=7;
    table=&sim_min1[0][0];                        
    ch=minimize;
    break;
    
  case '3':
    I=4; J=7;
    table=&sim_min2[0][0];                        
    ch=minimize;
    break;
    
  case '4':
    I=4; J=6;
    table=&dual_min[0][0];
    ch=dualmini;
    break;
            
  case '5':
    I=4; J=9;
    table=&cut_plane1[0][0];
    ch=minimize;
    //ch=integer;
    break;
        
  case '6':
    I=3; J=5;
    table=&cut_plane2[0][0];
    ch=maximize;
    //ch=integer;
    break;
        
  default: return halt;
  }

  diff=J-I;    
  if(diff<0) diff=(-diff);
  
  free(basic_var_tag);
  basic_var_tag=(char *)calloc(sizeof(char), I);
  
  Tagging;
  return ch;
}
